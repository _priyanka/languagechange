//
//  ViewController.swift
//  Localization
//
//  Created by Sierra 4 on 01/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import Localize_Swift

class ViewController: UIViewController {

    @IBOutlet var textName: UITextField!
    @IBOutlet var textS: UITextField!
    @IBOutlet var textCode: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       /* textName.text = NSLocalizedString("keyOne", comment: "Welcome to Code Brew Labs").localized()
         textS.text = NSLocalizedString("keyTwo", comment: "Welcome to Code Brew Labs").localized()
        textCode.text = NSLocalizedString("keyThree", comment: "Welcome to Code Brew Labs").localized()*/
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
    
        if (sender.selectedSegmentIndex == 0)
        {
            Localize.setCurrentLanguage("en")
            reloadEnglish()
            //reload()
        }
        else if (sender.selectedSegmentIndex == 1)
        {
            Localize.setCurrentLanguage("ar")
            reloadArabic()
            //reload()
        }
    }
    func reloadEnglish()
    {
        textName.text = "Priyanka Negi".localized()
        textName.textAlignment = .left
        textS.text = "Software Trainee".localized()
        textS.textAlignment = .left
        textCode.text = "Code Brew Labs".localized()
        textCode.textAlignment = .left
    }
    func reloadArabic()
    {
        textName.text = "بريانكا نيجي".localized()
        textName.textAlignment = .right
        textS.text = "متدرب البرمجيات".localized()
         textS.textAlignment = .right
        textCode.text = "كود المشروب مختبرات".localized()
         textCode.textAlignment = .right
    }
    /*func reload()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "viewControllerIdentifier")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }*/

}

